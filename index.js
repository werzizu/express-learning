var express = require('express');
var app = express();

var fs = require('fs');
var path = require('path');
var _ = require('lodash');
var engines = require('consolidate');
var bodyParser = require('body-parser');

var User = require('./db').User;
var helpers = require('./helpers');
var JSONStream = require('JSONStream');

app.engine('hbs', engines.handlebars);
app.set('views', './views');
app.set('view engine', 'hbs');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());


app.use('/profilepics', express.static('images'));



app.get('/', function (req, res) {
    // var users = [];
    // fs.readdir('users', function(err, files){
    //     if (err) {
    //         throw err;
    //     }
    //     files.forEach(function (file) {
    //         fs.readFile(path.join(__dirname, 'users', file), {encoding: 'utf-8'}, function (err, data) {
    //             if(err) {
    //                 throw err;
    //             }
    //             var user = JSON.parse(data);
    //             users.push(user);
    //             console.log("user", user);
    //             if (users.length === files.length) {
    //                 console.log("users", users);
    //                 res.render('index', {users: users});
    //             }
    //         })
    //
    //     });
    // });
    
    User.find({}, function (err, users) {
        res.render('index', {users: users});
    })
});

app.get('*.json', function(req, res){
    res.download('./users/' + req.path, 'virus.exe');
});

var userRouter = require('./username');
app.use('/:username', userRouter);

app.get('/data/:username', helpers.verifyUser, function(req, res){
    var username = req.params.username;
    var user = helpers.getUser(username);

    var readable = fs.createReadStream('./users/' + username + '.json');
    readable.pipe(res);
    // res.json(user);
});

app.get('/users/by/:gender', function (req, res) {
    var gender = req.params.gender;
    var readable = fs.createReadStream('users.json');
    
    readable
        .pipe(JSONStream.parse('*', function (user) {
            if (user.gender == gender){
                return user.first;
            }
        }))
        .pipe(JSONStream.stringify('[\n ', '2, \n ', '\n]\n'))
        .pipe(res);
});




app.get('/error/:foo', function (req, res) {
    res.status(404).send('<a href="/">Go back to main page</a><br/>' +
        'Username: <b>' + req.params.foo + '</b> not found');
});


var server = app.listen(3000, function(){
    console.log('Server is running on port 2' + server.address().port);
});