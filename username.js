var express = require('express');
var helpers = require('./helpers');
var User = require('./db').User;

var router = express.Router({
    mergeParams: true
});

router.use(function(req, res, next){
    console.log(req.method, 'for', req.params.username, " path = ", req.path);
    next();
});

router.get('/', helpers.verifyUser, function(req, res){
    var username = req.params.username;
    User.findOne({username: username}, function (err, user) {
       res.render('user', {user:user});
    });
});

router.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500).send("Something broke");
});

router.put('/', helpers.verifyUser, function (req, res) {
    var username = req.params.username;
    var user = helpers.getUser(username);
    User.findOneAndUpdate({username:username}, {first: req.body.first, second: user.second}, function(err, user){
        res.end();
    })
});

module.exports = router;
