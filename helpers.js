var path = require('path');
var fs = require('fs');

function getUserFilePath (username) {
    return path.join(__dirname, 'users', username) + '.json';
}

function getUser (username) {
    var user = JSON.parse(fs.readFileSync(getUserFilePath(username), {encoding: 'utf8'}));
    return user;
}

function saveUser (username, data){
    var fp = getUserFilePath(username);
    fs.unlinkSync(fp);
    fs.writeFileSync(fp, JSON.stringify(data, null, 2), {ecoding: 'utf-8'})
    console.log("saved");
}

function verifyUser(req, res, next){
    var fp = getUserFilePath(req.params.username);

    fs.exists(fp, function(yes){
        if(yes){
            next()
        } else {
            res.redirect('/error/' + req.params.username);
        }
    })
}

exports.getUserFilePath = getUserFilePath;
exports.getUser = getUser;
exports.saveUser = saveUser;
exports.verifyUser = verifyUser;
