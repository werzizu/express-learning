var uri = 'mongodb://localhost:27017/express-learning';
var _ = require('lodash');

//
// var MongoClient = require('mongodb').MongoClient
//
// var findUsers = function (db, callback) {
//     var cursor = db.collection('users').find();
//     cursor.each(function (err,doc) {
//         if (doc != null){
//             console.dir(doc);
//         } else {
//             callback();
//         }
//     })
// };
//
// MongoClient.connect(uri, function (err, db) {
//     findUsers(db, function () {
//         db.close();
//     })
// });

var mongoose = require('mongoose');
mongoose.connect(uri);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
    console.log("db connected");
});

var userSchema = mongoose.Schema({
    first   : String,
    second  : String,
    username: String,
    gender  : String
});

userSchema.virtual('fullname').get(function () {
   return _.startCase(this.first + ' ' + this.second);
});

exports.User = mongoose.model('User', userSchema);

